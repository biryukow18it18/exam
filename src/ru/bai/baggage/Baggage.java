package ru.bai.baggage;

public class Baggage {
    private String surname;
    private int slot;
    private double massa;

    Baggage(String surname, int slot, double mas) {
        this.surname = surname;
        this.slot = slot;
        this.massa = mas;
    }

    public Baggage() {
        this("не указано", 0, 0);
    }

    String getSurname() {
        return surname;
    }


    public int getSlot() {
        return slot;
    }


    public double getMas() {
        return massa;
    }
    boolean Clad() {
        return slot == 1 && massa <= 10;
    }
}