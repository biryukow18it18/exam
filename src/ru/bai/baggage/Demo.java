package ru.bai.baggage;

public class Demo {
    public static void main(String[] args) {

        Baggage passenger1 = new Baggage("Иванов", 1, 10);
        Baggage passenger2 = new Baggage("Воробьев", 1, 2);
        Baggage passenger3 = new Baggage("Соколов", 5, 35);
        Baggage passenger4 = new Baggage("Сергеев", 2, 20);
        Baggage passenger5 = new Baggage("Петров", 4, 30);

        Baggage[] passenger = {passenger1, passenger2, passenger3, passenger4, passenger5};

        pasClad(passenger);
    }

    private static void pasClad(Baggage[] bag) {
        for (Baggage baggage : bag) {
            if (baggage.Clad()) {
                System.out.println(baggage.getSurname() + " - С ручной кладью ");
            }
        }
    }
}

