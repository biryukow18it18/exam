
import  java.util.Scanner;
public class Array {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int[] array = new int[5];
        int result = 1;
        for (int i = 0; i < array.length; i++) {
            array[i] = sc.nextInt();
            if (i % 2 == 0) {
                result *= array[i];

            }

        }
        System.out.print("Произведение четных элементов массива = " + result);
    }
}